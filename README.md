<!--
SPDX-FileCopyrightText: 2024 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

Simple Twitch Live Notifications documentation © 2024 by Florian Warzecha is licensed under CC BY-SA 4.0.

-->

# Simple Twitch Live Notifications

Get notified when your favorite streamers go online with this small desktop app.

## Usage

1. Start the program once to find out where the program expects its config file.

2. Create the file. It has the following format:
```
(
	client_id: "",
	client_secret: "",
	streamers: ["streamer1", "streamer2"],
)
```

Note that the streamer names are *case-sensitive*.
To get a client_id and client_secret create a new application
in the [twitch developer console](https://dev.twitch.tv/console/apps).

3. (Optionally) put the program into autostart.

## Installation

Either:

- Clone the repository and install with `cargo install --path .`

or

- download the prebuilt binaries for the latest release at the releases overview [releases](https://gitlab.com/liketechnik/simple-twitch-live-notifications/-/releases) (if on linux or windows).

## Configuration option

By default there's a sleep period of 60 seconds between every request to the twitch api.
To modify this value, specify the `sleep_secs: 60,` option with a value of your choice in the config file.

## License

Except specific files which bear a different mention,
this programme is licensed under the [EUPL-1.2](
https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
).

Except specific files which bear a different mention,
this programme's documentation is licensed under [CC BY-SA 4.0](
http://creativecommons.org/licenses/by-sa/4.0
).
