// SPDX-FileCopyrightText: 2024 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: EUPL-1.2
//
// Copyright Florian Warzecha <liketechnik@disroot.org> 2024.
// Licensed under the EUPL-1.2.

use amethyst_config::Config;
use clap::Parser;
use directories::ProjectDirs;
use notify_rust::Notification;
use serde::{Deserialize, Serialize};
use twitch_api2::{
	helix::streams::GetStreamsRequest,
	twitch_oauth2::{AppAccessToken, ClientId, ClientSecret},
	HelixClient,
};

#[derive(Parser)]
#[command(name = "simple-twitch-live-notifications")]
#[command(version = clap::crate_version!(), long_version = long_version())]
#[command(author = "Florian Warzecha <liketechnik@disroot.org>")]
/// Get notified when your favorite streamers go online
/// with this small desktop app.
///
/// Periodically checks if any of the configured streamers
/// has gone live.
/// See the README in the
/// [repository](
/// https://gitlab.com/liketechnik/simple-twitch-live-notifications
/// ) for configuration options.
///
/// Except specific files which bear a different mention,
/// this programme is licensed under the [EUPL-1.2](
/// https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
/// ).
///
/// Except specific files which bear a different mention,
/// this programme's documentation is licensed under [CC BY-SA 4.0](
/// http://creativecommons.org/licenses/by-sa/4.0
/// ).
struct Cli {}

fn long_version() -> &'static str {
	concat!(
		env!("CARGO_PKG_VERSION"),
		"\n",
		"features: ",
		env!("VERGEN_CARGO_FEATURES"),
		"\n",
		"build timestamp: ",
		env!("VERGEN_BUILD_TIMESTAMP"),
		"\n",
		"describe: ",
		env!("VERGEN_GIT_DESCRIBE"),
		"\n",
		"commit sha: ",
		env!("VERGEN_GIT_SHA"),
		"\n",
		"commit timestamp: ",
		env!("VERGEN_GIT_COMMIT_TIMESTAMP"),
		"\n",
		"commit branch: ",
		env!("VERGEN_GIT_BRANCH"),
		"\n",
		"rustc semver: ",
		env!("VERGEN_RUSTC_SEMVER"),
		"\n",
		"rustc channel: ",
		env!("VERGEN_RUSTC_CHANNEL"),
		"\n",
		"rustc commit sha: ",
		env!("VERGEN_RUSTC_COMMIT_HASH"),
		"\n",
		"rustc host triple: ",
		env!("VERGEN_RUSTC_HOST_TRIPLE"),
		"\n",
		"cargo debug: ",
		env!("VERGEN_CARGO_DEBUG"),
		"\n",
		"cargo opt-level: ",
		env!("VERGEN_CARGO_OPT_LEVEL"),
		"\n",
		"cargo target-triple: ",
		env!("VERGEN_CARGO_TARGET_TRIPLE"),
		"\n",
		"build os: ",
		env!("VERGEN_SYSINFO_OS_VERSION"),
	)
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct TLNConfig {
	pub client_id: String,
	pub client_secret: String,
	pub streamers: Vec<String>,
	#[serde(default = "default_sleep_secs")]
	pub sleep_secs: u64,
}

fn default_sleep_secs() -> u64 {
	60
}

#[async_std::main]
async fn main() {
	let _ = Cli::parse();

	use std::error::Error;

	let mut live_map: std::collections::HashMap<String, bool> =
		std::collections::HashMap::new();

	let config = async_std::task::spawn_blocking(load_config).await;

	loop {
		for streamer in &config.streamers {
			if !live_map.contains_key(streamer) {
				live_map.insert(streamer.to_string(), false);
			}
		}
		live_map.retain(|streamer, _| config.streamers.contains(streamer));

		if let Err(err) = run(&config, &mut live_map).await {
			println!("Error: {}", err);
			let mut e: &'_ dyn Error = err.as_ref();
			while let Some(cause) = e.source() {
				println!("Caused by: {:?}", cause);
				e = cause;
			}
		}

		async_std::task::sleep(std::time::Duration::from_secs(
			config.sleep_secs,
		))
		.await;
	}
}

fn load_config() -> TLNConfig {
	let proj_dirs = ProjectDirs::from(
		"",
		"liketechnik",
		"Simple Twitch Live Notifications",
	)
	.unwrap();
	let mut config_path = proj_dirs.config_dir().to_path_buf();
	config_path.push("config.ron");
	if !config_path.is_file() {
		panic!(
			"Config file '{}' does not exist!",
			config_path.to_string_lossy()
		);
	}
	TLNConfig::load(config_path).unwrap()
}

async fn run(
	config: &TLNConfig,
	live_map: &mut std::collections::HashMap<String, bool>,
) -> Result<(), Box<dyn std::error::Error + Send + Sync + 'static>> {
	println!("Requesting data....");

	let client_id = ClientId::new(config.client_id.clone());
	let client_secret = ClientSecret::new(config.client_secret.clone());
	let scopes = vec![];

	let client: HelixClient<'_, surf::Client> = HelixClient::new();
	let token = AppAccessToken::get_app_access_token(
		&client,
		client_id,
		client_secret,
		scopes,
	)
	.await?;

	let is_live = GetStreamsRequest::builder()
		.user_login(
			config.streamers.iter().map(|s| s.as_str().into()).collect(),
		)
		.build();

	let response = client.req_get(is_live, &token).await?;

	for (streamer, online) in live_map.iter_mut() {
		if response
			.data
			.iter()
			.filter(|s| s.user_name.as_str() == streamer)
			.count() > 0
		{
			if !*online {
				println!("Sending notification for {}", streamer);
				Notification::new()
					.summary(&format!("{} went live!", streamer))
					.body(&format!("{} went live on Twitch.tv!", streamer))
					.show()?;
			}
			*online = true;
		} else {
			if *online {
				Notification::new()
					.summary(&format!("{} went offline!", streamer))
					.body(&format!("{} went offline on Twitch.tv!", streamer))
					.show()?;
			}
			*online = false;
		}
	}

	Ok(())
}
