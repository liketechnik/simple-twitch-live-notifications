# SPDX-FileCopyrightText: 2024 Florian Warzecha <liketechnik@disroot.org>
#
# SPDX-License-Identifier: CC0-1.0
#
# Development environment configuration © 2024 by Florian Warzecha
# is marked with CC0 1.0
{
  cargoMeta,
  pkgs,
  mkShell,
  fenixRustToolchain,
  bashInteractive,
  cargo-edit,
  reuse,
  just,
  eclint,
}:
mkShell {
  inputsFrom = [pkgs.${cargoMeta.package.name}];

  packages = [
    fenixRustToolchain

    bashInteractive

    # for upgrading dependencies (i.e. versions in Cargo.toml)
    cargo-edit

    reuse
    just
    eclint
  ];

  shellHook = ''
    unset SOURCE_DATE_EPOCH
    just --list --list-heading $'just <task>:\n'
  '';
}
