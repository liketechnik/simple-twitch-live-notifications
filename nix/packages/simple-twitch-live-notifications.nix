# SPDX-FileCopyrightText: 2024 Florian Warzecha <liketechnik@disroot.org>
#
# SPDX-License-Identifier: CC0-1.0
#
# Development environment configuration © 2024 by Florian Warzecha
# is marked with CC0 1.0
{
  lib,
  flake-self,
  cargoMeta,
  nix-filter,
  rustPlatform,
  dbus,
  pkg-config,
  openssl,
  curl,
}:
rustPlatform.buildRustPackage {
  pname = cargoMeta.package.name;
  version = cargoMeta.package.version;

  src = nix-filter {
    root = ../../.;
    include = [
      "src"
      "Cargo.toml"
      "Cargo.lock"
      "build.rs"
    ];
  };

  cargoLock.lockFile = ../../Cargo.lock;

  nativeBuildInputs = [
    pkg-config
    (lib.getDev curl)
  ];

  buildInputs = [
    dbus
    openssl
    curl
  ];

  VERGEN_IDEMPOTENT = "1";
  VERGEN_GIT_SHA =
    if flake-self ? "rev"
    then flake-self.rev
    else if flake-self ? "dirtyRev"
    then flake-self.dirtyRev
    else lib.warn "no git rev available" "NO_GIT_REPO";
  VERGEN_GIT_BRANCH =
    if flake-self ? "ref"
    then flake-self.ref
    else "";
  VERGEN_GIT_COMMIT_TIMESTAMP = flake-self.lastModifiedDate;
}
