# SPDX-FileCopyrightText: 2024 Florian Warzecha <liketechnik@disroot.org>
#
# SPDX-License-Identifier: CC0-1.0
#
# Development environment configuration © 2024 by Florian Warzecha
# is marked with CC0 1.0

build:
	cargo build

build-release:
	cargo build --release

run:
	cargo run

run-release:
	cargo run --release

format:
	cargo fmt --check
	eclint -exclude "{Cargo.lock,flake.lock,LICENSES/**}"

format-fix:
	cargo fmt
	eclint -exclude "{Cargo.lock,flake.lock,LICENSES/**}" --fix

lint:
	cargo clippy

lint-fix:
	cargo clippy --fix

reuse:
	reuse lint
